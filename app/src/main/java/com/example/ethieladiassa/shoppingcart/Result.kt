package com.example.ethieladiassa.shoppingcart

import com.google.gson.annotations.SerializedName

data class Result(
    @SerializedName("cat_list")
    var catList: List<CatList> = listOf(),

    @SerializedName("items_list")
   var itemsList: List<CatList> = listOf()
)