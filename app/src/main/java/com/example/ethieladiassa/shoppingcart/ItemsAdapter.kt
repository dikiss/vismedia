package com.example.ethieladiassa.shoppingcart

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.squareup.picasso.Picasso

class ItemsAdapter(var context: Context, var products: List<ItemsList> = arrayListOf()) :
    RecyclerView.Adapter<ItemsAdapter.ViewHolder>() {
    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ItemsAdapter.ViewHolder {
        // The layout design used for each list item
        val view = LayoutInflater.from(context).inflate(R.layout.item_list, null)
        return ViewHolder(view)

    }


    override fun onBindViewHolder(viewHolder: ItemsAdapter.ViewHolder, position: Int) {
        //we simply call the `bindProduct` function here

    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view) {


        }

    }

}