package com.example.ethieladiassa.shoppingcart

import com.google.gson.annotations.SerializedName

data class ItemsList(

    @SerializedName("id")
    var id: Int? = null,

    @SerializedName("cat_id")
    var cat_id: Int? = null,

    @SerializedName("media_id")
    var media_id: Int? = null,

    @SerializedName("fold")
    var fold: String? = null,

    @SerializedName("iron")
    var iron: String? = null,

    @SerializedName("hang")
    var hang: String? = null,

    @SerializedName("iconx1")
    var iconx1: String? = null,

    @SerializedName("iconx2")
    var iconx2: String? = null,

    @SerializedName("iconx3")
    var iconx3: String? = null,

    @SerializedName("title")
    var title: String? = null,

    @SerializedName("cat_title")
    var cat_title: String? = null



)