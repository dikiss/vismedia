package com.example.ethieladiassa.shoppingcart

import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*

interface APIService {

    @GET("cat_list")
    fun getCatList(
    ): Call<List<CatList>>

    @GET("items_list")
    fun getItemsList(
    ): Call<List<CatList>>

}
